Source: python-cluster
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Adrian Alves <aalves@gmail.com>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-pytest <!nocheck>,
 python3-setuptools,
Standards-Version: 4.6.1
Testsuite: autopkgtest-pkg-python
Homepage: https://github.com/exhuma/python-cluster/
Vcs-Git: https://salsa.debian.org/python-team/packages/python-cluster.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-cluster

Package: python3-cluster
Architecture: all
Depends:
 python3-pkg-resources,
 ${misc:Depends},
 ${python3:Depends},
Description: allows grouping a list of arbitrary objects into related groups (clusters)
 python-cluster is a "simple" package that allows users to create several groups
 (clusters) of objects from a list. It's meant to be flexible and able to
 cluster any object. To ensure this kind of flexibility, you need not only to
 supply the list of objects, but also a function that calculates the similarity
 between two of those objects. For simple datatypes, like integers, this can be
 as simple as a subtraction, but more complex calculations are possible. Right
 now, it is possible to generate the clusters using a hierarchical clustering
 and the popular K-Means algorithm. For the hierarchical algorithm there are
 different "linkage" (single, complete, average and uclus) methods available.
 .
 This is the Python 3 compatible package.
